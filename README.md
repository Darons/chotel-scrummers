## CHotel

Simple API for booking rooms build with in Nodejs using Expressjs, TypeScript and Mongo.
This is my first time trying clean architecture for Node, so it delayed me a little bit.

To run project use create and .env file with the following variables:

`PORT=3000`
`CORS_URL=*`
`DATABASE_URL = mongodb+srv://root:root@junior-camacho.ptmfp.mongodb.net/chotel?retryWrites=true&w=majority`

The database url is a Mongo cluster with 15 rooms added, numbered from 1 to 10, 5 of each type (1,2,3).

Then run `yarn start` on command line.
### Endpoints

* GET http://localhost:3000/api/room/availability/:type/:checkin:checkout

Returns a list with rooms available.

Example: http://localhost:3000/api/room/availability/3/2020-12-20/2020-12-22


* POST http://localhost:3000/api/room/reservation/:type/:checkin:checkout

Creates a resevation.

Example: http://localhost:3000/api/room/reservation/2/2020-12-25/2020-12-28

* PUT http://localhost:3000/api/room/reservation/:id/:type/:checkin:checkout/

Edits a reservation.

Example: http://localhost:3000/api/room/reservation/5fce938f740da643f2256fc7/1/2020-12-20/2020-12-23

* DELETE http://localhost:3000/api/room/reservation/:id

Deletes a reservation.

Example: http://localhost:3000/api/room/reservation/5fce938f740da643f2256fc7


### Notes:

I created a client model that is supposed to be inherited from a User model, but I did not create its CRUD for time isues.

I did not create protected routes with token because I did not implemented users login. I added jwt but didn't implement the middleware.

I did not made tests because I was afraid to suffer a blackout.
