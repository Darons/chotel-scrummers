export const port = process.env.PORT

export const db = process.env.DATABASE_URL

export const corsUrl = process.env.CORS_URL