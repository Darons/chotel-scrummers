import { model, Schema, Document } from 'mongoose'
import Client from './client'
export const DOCUMENT_NAME = 'Occupancy'
export const COLLECTION_NAME = 'Occupancies'

export default interface Occupancy extends Document {
  roomNo: number
  type: number
  client: number
  checkIn: Date
  checkOut: Date
  status?: boolean
  createdAt?: Date
  updatedAt?: Date
}

const schema = new Schema(
  {
    roomNo: {
      type: Schema.Types.Number,
      ref: 'Room',
      required: true,
    },
    type: {
        type: Schema.Types.Number,
        required: true,
      },
    client: {
      type: Schema.Types.Number,
      required: true,
    },
    checkIn: {
      type: Schema.Types.Date,
      required: true,
    },
    checkOut: {
      type: Schema.Types.Date,
      required: true,
    },
    typeNo: {
      type: Schema.Types.Number,
    },
    status: {
      type: Schema.Types.Boolean,
      default: true,
    },
    createdAt: {
      type: Date,
      required: true,
      default: new Date(),
      select: false,
    },
    updatedAt: {
      type: Date,
      required: true,
      default: new Date(),
      select: false,
    },
  },
  {
    versionKey: false,
  }
)

export const OccupancyModel = model<Occupancy>(
  DOCUMENT_NAME,
  schema,
  COLLECTION_NAME
)
