import { Request, Response } from 'express'
import { Types } from 'mongoose'
import Responses from '../helpers/responses'
import OccupancyRepo from '../domain/repository/occupancyRepo'
import RoomRepo from '../domain/repository/roomRepo'

export default class OccupancyController {
  public static async checkAvailability(req: Request, res: Response) {
    try {
      const totalRooms = await RoomRepo.findByType(req.params.type)
      console.log(totalRooms)
      const type = parseInt(req.params.type)
      const checkIn = new Date(req.params.checkin)
      const checkOut = new Date(req.params.checkout)
      const occupied = await OccupancyRepo.findOccupied(type, checkIn, checkOut)
      if (totalRooms.length == occupied.length) {
        Responses.throwError('Not available')
      }
      const availableRooms = OccupancyController.roomsAvailable(totalRooms, occupied)
      Responses.makeResponseOk(res, availableRooms)
    } catch (e) {
      Responses.makeResponseException(res, e)
    }
  }

  public static async makeReservation(req: Request, res: Response) {
    try {
      const reservation = await OccupancyRepo.create({
        type: parseInt(req.params.type),
        checkIn: new Date(req.params.checkin),
        checkOut: new Date(req.params.checkout),
        roomNo: req.body.roomNo,
        client: req.body.client,
      })
      console.log(reservation)
      Responses.makeResponseOk(res, reservation)
    } catch (e) {
      Responses.makeResponseException(res, e)
    }
  }

  public static async updateReservation(req: Request, res: Response) {
    try {
      const id = Types.ObjectId(req.params.id)
      const reservation = await OccupancyRepo.findById(id)
      if (!reservation) Responses.throwError('Reservation not registered')
      ;(reservation.type = parseInt(req.params.type)),
        (reservation.checkIn = new Date(req.params.checkin)),
        (reservation.checkOut = new Date(req.params.checkout))
      const newReservation = await OccupancyRepo.updateInfo(reservation)
      Responses.makeResponseOk(res, reservation)
    } catch (e) {
      Responses.makeResponseException(res, e)
    }
  }

  public static async cancelReservation(req: Request, res: Response) {
    try {
      const id = Types.ObjectId(req.params.id)
      const reservation = await OccupancyRepo.findById(id)
      if (!reservation) Responses.throwError('Reservation not registered')
      await OccupancyRepo.delete(id)
      Responses.makeResponseOk(res, reservation)
    } catch (e) {
      Responses.makeResponseException(res, e)
    }
  }

  private static roomsAvailable(rooms: any, occupied: any) {
    const totalRooms = rooms.map((x: any) => x.roomNo)
    const occupiedRooms = occupied.map((x: any) => x.roomNo)

    return totalRooms.filter((no: any) => {
      return !occupiedRooms.includes(no)
    })
  }
}
