import { Request, Response } from "express"
import Responses from "../helpers/responses"
import RoomRepo from "../domain/repository/roomRepo"

export default class RoomController {
    public static async create(req: Request, res: Response) {
        try {
          const room = await RoomRepo.create(req.body)
          Responses.makeResponseOk(res, room)
        } catch (e) {
          Responses.makeResponseException(res, e)
        }
      }
}
