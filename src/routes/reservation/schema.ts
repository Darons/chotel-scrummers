import Joi from '@hapi/joi'

export default {
  reservation: Joi.object().keys({
    type: Joi.number().required(),
    checkin: Joi.date().min('now').required(),
    checkout: Joi.date().required(),
  }),
  reservationId: Joi.object().keys({
    id: Joi.string().required(),
    type: Joi.number().required(),
    checkin: Joi.date().min('now').required(),
    checkout: Joi.date().required(),
  }),
  id: Joi.object().keys({
    id: Joi.string().required(),
  })
}
