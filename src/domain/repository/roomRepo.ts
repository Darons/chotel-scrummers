import { Types } from 'mongoose'
import Room, { RoomModel } from '../models/room'

export default class RoomRepo {
  public static findById(id: Types.ObjectId): Promise<any> {
    return RoomModel.findOne({ _id: id, status: true }).lean().exec()
  }

  public static findByType(type: any): Promise<any> {
    return RoomModel.find({ type: type, status: true }).lean().exec()
  }

  public static async create(room: any): Promise<any> {
    const createdRoom = await RoomModel.create(room)
    return createdRoom
  }

  public static updateInfo(room: any): Promise<any> {
    room.updatedAt = new Date()
    return RoomModel.updateOne({ _id: room._id }, { $set: { ...room } })
      .lean()
      .exec()
  }
}
