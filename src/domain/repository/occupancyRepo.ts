import { Types } from 'mongoose'
import Occupancy, { OccupancyModel } from '../models/occupancy'

export default class OccupancyRepo {
  public static async findOccupied(
    type: number,
    checkIn: Date,
    checkOut: Date
  ): Promise<any> {
    return OccupancyModel.find({
      type: type,
      $or: [
        {
          $and: [
            { checkIn: { $gte: checkIn } },
            { checkIn: { $lt: checkOut } },
          ],
        },
        {
          $and: [
            { checkOut: { $gt: checkIn } },
            { checkOut: { $lte: checkOut } },
          ],
        },
      ],
    })
      .lean()
      .exec()
  }

  public static async create(occupancy: any): Promise<any> {
    const createdOccupancy = await OccupancyModel.create(occupancy)
    return createdOccupancy
  }

  public static updateInfo(occupancy: Occupancy): Promise<any> {
    occupancy.updatedAt = new Date()
    return OccupancyModel.updateOne(
      { _id: occupancy._id },
      { $set: { ...occupancy } }
    )
      .lean()
      .exec()
  }

  public static delete(id: Types.ObjectId): Promise<any> {
    return OccupancyModel.deleteOne({ _id: id }).exec()
  }

  public static findById(id: Types.ObjectId): Promise<any> {
    return OccupancyModel.findById(id).lean().exec()
  }
}
