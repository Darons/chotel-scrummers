import { Response } from "express"

export default class Responses {
  public static makeResponseOk(res: Response, data: any) {
    res.status(200).json(data)
  }

  public static makeResponseException(res: Response, error: any) {
    const json = {
        code: error.code,
        message: error.message
    }
    res.status(200).json(json)
  }
  public static throwError (message: string) {
    throw Object.assign(
      new Error(message),
      { code: 401 }
    )
  }
  
}
