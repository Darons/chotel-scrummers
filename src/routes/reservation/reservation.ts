import express from 'express'
import OccupancyController from '../../controllers/occupancy'
import validators, { ValidationSource } from '../../middleware/validators'
import schema from './schema'
import RoomController from '../../controllers/room'

const router = express.Router()

router.get(
  '/availability/:type/:checkin/:checkout',
  validators(schema.reservation, ValidationSource.PARAM),
  OccupancyController.checkAvailability
)

router.post(
  '/reservation/:type/:checkin/:checkout',
  validators(schema.reservation, ValidationSource.PARAM),
  OccupancyController.makeReservation
)

router.put(
  '/reservation/:id/:type/:checkin/:checkout',
  validators(schema.reservationId, ValidationSource.PARAM),
  OccupancyController.updateReservation
)

router.delete(
  '/reservation/:id',
  validators(schema.id, ValidationSource.PARAM),
  OccupancyController.cancelReservation
)

router.post('/', RoomController.create)


export default router
