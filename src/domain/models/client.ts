import { model, Schema, Document } from 'mongoose'
//This is supposed to be a model extending and User or Person model.
export const DOCUMENT_NAME = 'Client'
export const COLLECTION_NAME = 'Clients'

export default interface Client extends Document {
  name: string
  email: string
  dni: string
  status?: boolean
  createdAt?: Date
  updatedAt?: Date
}

const schema = new Schema(
  {
    name: {
      type: Schema.Types.String,
      maxlength: 100,
    },
    email: {
      type: Schema.Types.String,
      required: true,
      unique: true,
    },
    dni: {
      type: Schema.Types.Number,
      required: true,
    },
    status: {
      type: Schema.Types.Boolean,
      default: true,
    },
    createdAt: {
      type: Date,
      required: true,
      select: false,
      default: new Date()
    },
    updatedAt: {
      type: Date,
      required: true,
      select: false,
      default: new Date()
    },
  },
  {
    versionKey: false,
  }
)

export const ClientModel = model<Client>(DOCUMENT_NAME, schema, COLLECTION_NAME)
