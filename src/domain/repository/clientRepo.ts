import { Types } from 'mongoose'
import Client, { ClientModel } from '../models/client'

export default class ClientRepo {
  public static findById(id: Types.ObjectId): Promise<any> {
    return ClientModel.findOne({ _id: id, status: true }).lean().exec()
  }

  public static async create(client: Client): Promise<any> {
    const createdClient = await ClientModel.create(client)
    return createdClient
  }

  public static updateInfo(client: Client): Promise<any> {
    client.updatedAt = new Date()
    return ClientModel.updateOne({ _id: client._id }, { $set: { ...client } })
      .lean()
      .exec()
  }
}
