import { model, Schema, Document } from 'mongoose'

export const DOCUMENT_NAME = 'Room'
export const COLLECTION_NAME = 'Rooms'

export default interface Room extends Document {
  roomNo: number
  phoneNo?: string
  type: number
  status?: boolean
  createdAt?: Date
  updatedAt?: Date
}

const schema = new Schema(
  {
    roomNo: {
      type: Schema.Types.Number,
    },
    phoneNo: {
      type: Schema.Types.String,
    },
    type: {
      type: Schema.Types.Number,
    },
    status: {
      type: Schema.Types.Boolean,
      default: true,
    },
    createdAt: {
      type: Date,
      select: false,
      default: new Date()
    },
    updatedAt: {
      type: Date,
      select: false,
      default: new Date()
    },
  },
  {
    versionKey: false,
  }
)

export const RoomModel = model<Room>(DOCUMENT_NAME, schema, COLLECTION_NAME)
