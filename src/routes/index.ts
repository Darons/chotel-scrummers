import express from 'express'
import reservation from './reservation/reservation'

const router = express.Router()

router.use('/room', reservation)


export default router
