import express, { Request, Response, NextFunction } from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import { corsUrl } from './config'
import './domain' // initialize database
import router from './routes'

const app = express()

app.use(bodyParser.json({ limit: '10mb' }))

app.use(
  bodyParser.urlencoded({
    limit: '10mb',
    extended: true,
    parameterLimit: 50000,
  })
)

app.use(cors({ origin: corsUrl, optionsSuccessStatus: 200 }))

// Routes
app.use('/api', router)



export default app
